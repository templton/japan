<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Address
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $unit_id
 * @property string|null $title
 * @property string|null $coord
 * @property string|null $description
 * @method static \Illuminate\Database\Eloquent\Builder|Address newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Address newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Address query()
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereCoord($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereUnitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Address whereUpdatedAt($value)
 */
	class Address extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\City
 *
 * @property int $id
 * @property string $title
 * @property int $country_id
 * @property int $is_active
 * @property-read \App\Models\Country $country
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Unit[] $units
 * @property-read int|null $units_count
 * @method static \Illuminate\Database\Eloquent\Builder|City newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|City newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|City query()
 * @method static \Illuminate\Database\Eloquent\Builder|City whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|City whereTitle($value)
 */
	class City extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Country
 *
 * @property int $id
 * @property string $title
 * @property int $is_active
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\City[] $cities
 * @property-read int|null $cities_count
 * @method static \Illuminate\Database\Eloquent\Builder|Country newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Country newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Country query()
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Country whereTitle($value)
 */
	class Country extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Customer
 *
 * @property int $id
 * @property string|null $name
 * @property string $phone
 * @property string|null $phone_verified_at
 * @property string|null $code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Customer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer wherePhoneVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Customer whereUpdatedAt($value)
 */
	class Customer extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Exchange
 *
 * @property int $id
 * @property string $date
 * @property float $rate
 * @method static \Illuminate\Database\Eloquent\Builder|Exchange newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Exchange newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Exchange query()
 * @method static \Illuminate\Database\Eloquent\Builder|Exchange whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exchange whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Exchange whereRate($value)
 */
	class Exchange extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Order
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $status_id
 * @property int $customer_id
 * @property int $address_id
 * @property int $user_id
 * @method static \Illuminate\Database\Eloquent\Builder|Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereCustomerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Order whereUserId($value)
 */
	class Order extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\OrderProductExtensionItems
 *
 * @property int $id
 * @property int $order_product_id
 * @property int $product_extension_item_id
 * @property string $value
 * @method static \Illuminate\Database\Eloquent\Builder|OrderProductExtensionItems newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderProductExtensionItems newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderProductExtensionItems query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderProductExtensionItems whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderProductExtensionItems whereOrderProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderProductExtensionItems whereProductExtensionItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderProductExtensionItems whereValue($value)
 */
	class OrderProductExtensionItems extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\OrderProducts
 *
 * @property int $id
 * @property int $order_id
 * @property int $count
 * @method static \Illuminate\Database\Eloquent\Builder|OrderProducts newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderProducts newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderProducts query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderProducts whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderProducts whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderProducts whereOrderId($value)
 */
	class OrderProducts extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Product
 *
 * @property int $id
 * @property string $title
 * @property string $subtitle
 * @property string $image
 * @property int $product_group_id
 * @property float $base_price
 * @property int $is_vegan
 * @property int $is_hot
 * @property int $is_active
 * @property-read \App\Models\ProductGroup|null $group
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductUpsale[] $upsales
 * @property-read int|null $upsales_count
 * @method static \Illuminate\Database\Eloquent\Builder|Product newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereBasePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereIsHot($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereIsVegan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereProductGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereSubtitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Product whereTitle($value)
 */
	class Product extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProductExtension
 *
 * @property int $id
 * @property int $product_group_id
 * @property string $title
 * @property int $required
 * @property string $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductExtensionItem[] $items
 * @property-read int|null $items_count
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtension newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtension newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtension query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtension whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtension whereProductGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtension whereRequired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtension whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtension whereType($value)
 */
	class ProductExtension extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProductExtensionItem
 *
 * @property int $id
 * @property int $product_extension_id
 * @property string $value
 * @property float $affect_to_price
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtensionItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtensionItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtensionItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtensionItem whereAffectToPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtensionItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtensionItem whereProductExtensionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductExtensionItem whereValue($value)
 */
	class ProductExtensionItem extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProductGroup
 *
 * @property int $id
 * @property string $title
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductExtension[] $extensions
 * @property-read int|null $extensions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|ProductGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductGroup whereTitle($value)
 */
	class ProductGroup extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ProductUpsale
 *
 * @property int $id
 * @property int $product_id
 * @property int $upsale_product_id
 * @property-read \App\Models\Product|null $product
 * @method static \Illuminate\Database\Eloquent\Builder|ProductUpsale newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductUpsale newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductUpsale query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductUpsale whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductUpsale whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductUpsale whereUpsaleProductId($value)
 */
	class ProductUpsale extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Status
 *
 * @property int $id
 * @property string $title
 * @method static \Illuminate\Database\Eloquent\Builder|Status newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Status newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Status query()
 * @method static \Illuminate\Database\Eloquent\Builder|Status whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Status whereTitle($value)
 */
	class Status extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Task
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Task newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Task newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Task query()
 */
	class Task extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Todo
 *
 * @method static \Illuminate\Database\Eloquent\Builder|Todo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Todo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Todo query()
 */
	class Todo extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Unit
 *
 * @property int $id
 * @property int $city_id
 * @property string $address
 * @property string $coord
 * @property string $phone
 * @property int $is_active
 * @property-read \App\Models\City $city
 * @method static \Illuminate\Database\Eloquent\Builder|Unit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Unit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Unit query()
 * @method static \Illuminate\Database\Eloquent\Builder|Unit whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Unit whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Unit whereCoord($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Unit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Unit whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Unit wherePhone($value)
 */
	class Unit extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string $photo
 * @property int $unit_id
 * @property int $is_courier
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Task[] $tasks
 * @property-read int|null $tasks_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Todo[] $todos
 * @property-read int|null $todos_count
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereIsCourier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUnitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 */
	class User extends \Eloquent implements \Tymon\JWTAuth\Contracts\JWTSubject {}
}

