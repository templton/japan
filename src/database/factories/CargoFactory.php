<?php

namespace Database\Factories;

use App\Models\Cargo;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CargoFactory extends Factory
{


    protected $model = Cargo::class;

    public function definition()
    {
        return [
            'title' => $this->faker->title,
            'statusId' => rand(1,12),
            'exportMethodId' => rand(1,3),
            'cargoTypeId' => rand(1,4),
            'senderPartnerId' => rand(1,200),
            'clientPartnerId' => rand(1,200),
            'receiverPartnerId' => rand(1,200),
            'customsPartnerId' => rand(1,200),
            'voyageId' => rand(1,2) === 1 ? rand(1,6) : null,
            'description' => $this->faker->paragraph,
            'photos' => json_encode([["title" => "Faker1", "url" => $this->faker->imageUrl()],
                ["title" => "Faker2", "url" => $this->faker->imageUrl()],
            ]),
            'extraFields' => json_encode([
                "frame" => ["title" => ["en" => "Frame", "ru" => "Номер кузова"], "type" => "string", "required" => false, "value" =>  rand(1,2) === 1 ? Str::random(10) : null],
                "barcode" => ["title" => ["en" => "Barcode", "ru" => "Штрих-код"], "type" => "barcode", "required" => false, "value" => rand(1,2) === 1 ? rand(10000000,99999999): null],
                "width" => ["title" => ["en" => "Width", "ru" => "Ширина"], "type" => "number", "required" => true, "value" => rand(25,45)/10],
                "length" => ["title" => ["en" => "Length", "ru" => "Длина"], "type" => "number", "required" => true, "value" => rand(45,65)/10],
                "height" => ["title" => ["en" => "Height", "ru" => "Высота"], "type" => "number", "required" => true, "value" => rand(5,25)/10],
                "weight" => ["title" => ["en" => "Weight", "ru" => "Вес"], "type" => "number", "required" => true, "value" => rand(1000,25000)/10],
                "price" => ["title" => ["en" => "Price", "ru" => "Стоимость"], "type" => "number", "required" => true, "value" => rand(20,10000)*10],
                "parentCargo" => ["title" => ["en" => "Parent cargo", "ru" => "Родительский груз"], "type" => "entity", "required" => false, "value" => null],
                "subCargo" => ["title" => ["en" => "Sub cargo", "ru" => "Дочерние грузы"], "type" => "entities", "required" => false, "value" => null],
            ]),
            'tags' => json_encode([1,]),
            'ownerUserId' => rand(1,3),
        ];
    }
}
