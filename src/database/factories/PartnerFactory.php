<?php

namespace Database\Factories;

use App\Models\Partner;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class PartnerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Partner::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $tags = [1,];
        for ($i = 2; $i < 5; $i++) {
            if (rand(0, 1)) {
                array_push($tags, $i);
            }
        }
        return [
            'title' => $this->faker->name,
            'description' => $this->faker->paragraph,
            'photos' => json_encode([["title" => "Faker", "url" => $this->faker->imageUrl(150, 150, "people")],]),
            'extraFields' => json_encode([
                "email" => ["title" => ["en" => "Email", "ru" => "Электронная почта"], "type" => "email", "required" => false, "value" => $this->faker->unique()->safeEmail],
                "phone" => ["title" => ["en" => "Phone", "ru" => "Телефон"], "type" => "phone", "required" => false, "value" => rand(1, 2) === 1 ? $this->faker->phoneNumber : null],
                "whatsapp" => ["title" => ["en" => "WhatsApp", "ru" => "WhatsApp"], "type" => "whatsapp", "required" => false, "value" => rand(1, 2) === 1 ? $this->faker->phoneNumber : null],
                "telegram" => ["title" => ["en" => "Telegram", "ru" => "Telegram"], "type" => "telegram", "required" => false, "value" => $this->faker->unique()->userName],
                "viber" => ["title" => ["en" => "Viber", "ru" => "Viber"], "type" => "viber", "required" => false, "value" => rand(1, 2) === 1 ? $this->faker->phoneNumber : null],
            ]),
            'tags' => json_encode($tags),
            'ownerUserId' => rand(1,3),
        ];
    }
}
