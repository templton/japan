<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSearchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('searches', function (Blueprint $table) {
            $table->id();
            $table->boolean('show')->default(true);
            $table->string('query');
            $table->foreignId('entityTypeId')->default(2);
            $table->foreignId('entityId')->nullable();
            $table->unsignedBigInteger('counter')->nullable();
            $table->foreign('entityTypeId')->references('id')->on('entities');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('searches');
    }
}
