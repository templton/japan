<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Query\Expression;
class CreatePortsTable extends Migration
{
    public function up()
    {
        Schema::create('ports', function (Blueprint $table) {
            $table->id();
            $table->boolean('show')->default(true);
            $table->dateTime('created_at')->useCurrent();
            $table->dateTime('updated_at')->useCurrentOnUpdate()->default(now());
            $table->foreignId('createdById')->default(1);
            $table->foreignId('updatedById')->default(1);
            $table->foreign('createdById')->references('id')->on('users');
            $table->foreign('updatedById')->references('id')->on('users');

            $table->jsonb('title')->default(new Expression('(JSON_OBJECT())'));
            $table->jsonb('country')->default(new Expression('(JSON_OBJECT())'));
            $table->text('description')->nullable();
            $table->jsonb('photos')->default(new Expression('(JSON_ARRAY())'));
            $table->jsonb('extraFields')->default(new Expression('(JSON_ARRAY())'));
            $table->jsonb('documents')->default(new Expression('(JSON_ARRAY())'));
            $table->jsonb('tags')->default(new Expression('(JSON_ARRAY())'));



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ports');
    }
}
