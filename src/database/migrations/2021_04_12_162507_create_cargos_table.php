<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Query\Expression;
class CreateCargosTable extends Migration
{
    public function up()
    {
        Schema::create('cargos', function (Blueprint $table) {
            $table->id();
            $table->boolean('show')->default(true);
            $table->dateTime('created_at')->useCurrent();
            $table->dateTime('updated_at')->useCurrentOnUpdate()->default(now());
            $table->foreignId('createdById')->default(1);
            $table->foreignId('updatedById')->default(1);
            $table->foreignId('ownerUserId')->default(1);
            $table->foreign('createdById')->references('id')->on('users');
            $table->foreign('updatedById')->references('id')->on('users');
            $table->foreign('ownerUserId')->references('id')->on('users');

            $table->string('title')->default('Unnamed');
            $table->text('description')->nullable();
            $table->jsonb('photos')->default(new Expression('(JSON_ARRAY())'));
            $table->jsonb('extraFields')->default(new Expression('(JSON_ARRAY())'));
            $table->jsonb('documents')->default(new Expression('(JSON_ARRAY())'));
            $table->jsonb('tags')->default(new Expression('(JSON_ARRAY())'));

            $table->foreignId('statusId')->default(1);
            $table->foreignId('exportMethodId')->default(1);
            $table->foreignId('cargoTypeId')->default(1);
            $table->foreignId('senderPartnerId')->nullable();
            $table->foreignId('clientPartnerId')->nullable();
            $table->foreignId('receiverPartnerId')->nullable();
            $table->foreignId('customsPartnerId')->nullable();
            $table->foreignId('voyageId')->nullable();

            $table->foreign('statusId')->references('id')->on('cargo_statuses');
            $table->foreign('exportMethodId')->references('id')->on('export_methods');
            $table->foreign('cargoTypeId')->references('id')->on('cargo_types');
            $table->foreign('voyageId')->references('id')->on('voyages');
            $table->foreign('senderPartnerId')->references('id')->on('partners');
            $table->foreign('clientPartnerId')->references('id')->on('partners');
            $table->foreign('receiverPartnerId')->references('id')->on('partners');
            $table->foreign('customsPartnerId')->references('id')->on('partners');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargos');
    }
}
