<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCargoStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cargo_statuses', function (Blueprint $table) {
            $table->id();
            $table->boolean('show')->default(true);
            $table->dateTime('created_at')->useCurrent();
            $table->dateTime('updated_at')->useCurrentOnUpdate()->default(now());
            $table->foreignId('createdById')->default(1);
            $table->foreignId('updatedById')->default(1);
            $table->foreign('createdById')->references('id')->on('users');
            $table->foreign('updatedById')->references('id')->on('users');
            $table->string('color')->default('#000');
            $table->string('icon')->default('vinyl');
            $table->jsonb('title');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargo_statuses');
    }
}
