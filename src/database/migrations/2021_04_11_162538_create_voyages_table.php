<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Query\Expression;
class CreateVoyagesTable extends Migration
{

    public function up()
    {
        Schema::create('voyages', function (Blueprint $table) {
            $table->id();
            $table->boolean('show')->default(true);
            $table->dateTime('created_at')->useCurrent();
            $table->dateTime('updated_at')->useCurrentOnUpdate()->default(now());
            $table->foreignId('createdById')->default(1);
            $table->foreignId('updatedById')->default(1);
            $table->foreignId('ownerUserId')->default(1);
            $table->foreign('createdById')->references('id')->on('users');
            $table->foreign('updatedById')->references('id')->on('users');
            $table->foreign('ownerUserId')->references('id')->on('users');

            $table->string('title')->default('Unnamed');
            $table->text('description')->nullable();
            $table->jsonb('extraFields')->default(new Expression('(JSON_ARRAY())'));
            $table->jsonb('documents')->default(new Expression('(JSON_ARRAY())'));
            $table->jsonb('tags')->default(new Expression('(JSON_ARRAY())'));

            $table->foreignId('srcPortId')->nullable();
            $table->dateTime('departDate')->nullable();
            $table->foreignId('destPortId')->nullable();
            $table->dateTime('arrivalDate')->nullable();
            $table->foreignId('statusId')->default(1);
            $table->foreignId('shipId')->nullable();

            $table->foreign('srcPortId')->references('id')->on('ports');
            $table->foreign('destPortId')->references('id')->on('ports');
            $table->foreign('statusId')->references('id')->on('voyage_statuses');
            $table->foreign('shipId')->references('id')->on('ships');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voyages');
    }
}
