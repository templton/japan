<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Query\Expression;
use Carbon\Carbon;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->boolean('show')->default(true);
            $table->dateTime('created_at')->useCurrent();
            $table->dateTime('updated_at')->useCurrentOnUpdate()->default(now());
            $table->foreignId('createdById')->default(1);
            $table->foreignId('updatedById')->default(1);
            $table->dateTime('activeTo')->default(Carbon::now()->addYear());

            $table->string('name');
            $table->string('email')->unique();
            $table->dateTime('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->jsonb('settings')->default(new Expression('(JSON_OBJECT())'));
            $table->jsonb('permissions')->default(new Expression('(JSON_ARRAY())'));;
            $table->text('description')->nullable();
            $table->jsonb('photos')->default(new Expression('(JSON_ARRAY())'));
            $table->jsonb('extraFields')->default(new Expression('(JSON_ARRAY())'));
            $table->jsonb('documents')->default(new Expression('(JSON_ARRAY())'));
            $table->jsonb('tags')->default(new Expression('(JSON_ARRAY())'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
