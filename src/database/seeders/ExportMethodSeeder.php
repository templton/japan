<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExportMethodSeeder extends Seeder
{

    public function run()
    {
        DB::table('export_methods')->insert(['icon' => 'whole', 'title' => json_encode(["en" => "Whole", "ru" => "Полная пошлина"]),  'rate' => 1]);
        DB::table('export_methods')->insert(['icon' => 'disassembled', 'title' => json_encode(["en" => "Disassembled", "ru" => "Разбор"]),  'rate' => 1.3]);
        DB::table('export_methods')->insert(['icon' => 'dissected', 'title' => json_encode(["en" => "Dissected", "ru" => "Распил"]),  'rate' => 1.3]);

    }
}
