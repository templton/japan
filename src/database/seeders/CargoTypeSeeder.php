<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CargoTypeSeeder extends Seeder
{
    public function run()
    {
        DB::table('cargo_types')->insert(['icon' => 'car', 'title' => json_encode(["en" => "Car", "ru" => "Легковой автомобиль",]), ]);
        DB::table('cargo_types')->insert(['icon' => 'truck', 'title' => json_encode(["en" => "Truck", "ru" => "Грузовой автомобиль",]), ]);
        DB::table('cargo_types')->insert(['icon' => 'special', 'title' => json_encode(["en" => "Special", "ru" => "Спецтехника",]), ]);
        DB::table('cargo_types')->insert(['icon' => 'spare', 'title' => json_encode(["en" => "Spare", "ru" => "Запчасти",]), ]);


    }
}
