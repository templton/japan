<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ExtraFieldTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("extra_field_types")->insert(["code" => "string", "title" => json_encode(["en" => "string",            "ru" => "Строка",]),               "icon" => "pencil-alt",             "originalType" => "string", "model" => json_encode(
            [
                "readonly" => false,
                "placeholder" => "",
                "mask" => "",
                "length" => ["max" => 190, "min" => 0],
                "value" => "string",
                ]),]);
        DB::table("extra_field_types")->insert(["code" => "text", "title" => json_encode(["en" => "Text area", "ru" => "Текстовая область", ]),   "icon" => "comment-alt",            "originalType" => "string", "model" => json_encode(
            [
                "readonly" => false,
                "placeholder" => "",
                "length" => ["max" => 65535, "min" => 0],
                "markdown" => true,
                "value" => "string",
                ]),]);
        DB::table("extra_field_types")->insert(["code" => "number", "title" => json_encode(["en" => "Number",            "ru" => "Число",]),                "icon" => "calculator",             "originalType" => "number", "model" => json_encode(
            [
                "readonly" => false,
                "min" => -2147483647, //TODO: sizes
                "max" => 2147483647,
                "increment" => 1,
                "decimal" => 2,
                "locale" => false, //TODO: .toLocaleString()
                "value" => 1,
            ]),]);
        DB::table("extra_field_types")->insert(["code" => "checkbox", "title" => json_encode(["en" => "Checkbox", "ru" => "Галка",]),                "icon" => "check-circle", "originalType" => "boolean", "model" => json_encode(
            [
                "readonly" => false,
                "value" => false, // false || true
            ]),]);
        DB::table("extra_field_types")->insert(["code" => "switch", "title" => json_encode(["en" => "Switch",            "ru" => "Переключатель",]),        "icon" => "toggle-off",             "originalType" => "string", "model" => json_encode(
            [
                "readonly" => false,
                "values" => ["value on", "value off"],
                "value" => "value on", // "value on" || "value off"

            ]),]);
        DB::table("extra_field_types")->insert(["code" => "list", "title" => json_encode(["en" => "List",              "ru" => "Список", ]),              "icon" => "tasks",                  "originalType" => "string", "model" => json_encode(
            [
                "readonly" => false,
                "multi" => false,
                "values" => ["value1", "value2", "value3",],
                "editable" => false,
                "value" => ["value1", "value2", ]
                ]),]);
        DB::table("extra_field_types")->insert(["code" => "datetime", "title" => json_encode(["en" => "DateTime", "ru" => "Дата/Время",]),           "icon" => "clock",                  "originalType" => "string", "model" => json_encode(
            [
                "readonly" => false,
                "format" => "DD.MM.YY H:i:s",
                "value" => "DD.MM.YY H:i:s", //timestamp
                ]),]);
        DB::table("extra_field_types")->insert(["code" => "entity", "title" => json_encode(["en" => "Entity",            "ru" => "Сущность",]),             "icon" => "link",                   "originalType" => "string", "model" => json_encode(
            [
                "readonly" => false,
                "entityId" => 1, //Entity!
                "value" => ["id" => 1, "title" => "title of entity"],
                ]),]);
        DB::table("extra_field_types")->insert(["code" => "coord", "title" => json_encode(["en" => "Coordinates", "ru" => "Координаты",]),           "icon" => "map-marker-alt", "originalType" => "string", "model" => json_encode(
            [
                "readonly" => false,
                "value" => ["lat" => 12.123, "lng" => 12.123],//TODO: lat/lng
                ]),]);
        DB::table("extra_field_types")->insert(["code" => "file", "title" => json_encode(["en" => "File",              "ru" => "Файл",]),                 "icon" => "file-alt",               "originalType" => "string", "model" => json_encode(
            [
                "readonly" => false,
                "types" => ["pdf", "doc", "docx", "xls", "xlsx", "jpg", "png", "ext", "avi", "mp4", "mp3", ],
                "maxFileSize" => 1024, //TODO: size
                "multi" => false,
                "value" => [1,2,3,4,5,], // ids of files in files_table
                ]),]);
        DB::table("extra_field_types")->insert(["code" => "document", "title" => json_encode(["en" => "Document", "ru" => "Документ по шаблону",]),  "icon" => "file-invoice", "originalType" => "string", "model" => json_encode(
            [
                "readonly" => false,
                "types" => ["pdf", "doc", "docx", "xls", "xlsx", ],
                "maxFileSize" => 1024, //TODO: size
                "multi" => false,
                "value" => [1,2,3,4,5,], // ids of files in files_table
                ]),]);
        DB::table("extra_field_types")->insert(["code" => "image", "title" => json_encode(["en" => "Image",             "ru" => "Изображение",]),          "icon" => "file-image",             "originalType" => "string", "model" => json_encode(
            [
                "readonly" => false,
                "types" => ["jpg", "png", "gif", ],
                "maxFileSize" => 1024, //TODO: size
                "preview" => ["maxWidth" => 100, "maxHeight" => 100,],
                "size" => ["maxWidth" => 100, "maxHeight" => 100,],
                "default" => "/img/no-image.png",
                "multi" => true,
                "value" => [1,2,3,4,5,], // ids of files in files_table
                ]),]);
        DB::table("extra_field_types")->insert(["code" => "barcode", "title" => json_encode(["en" => "Barcode", "ru" => "Штрих-код",]),            "icon" => "barcode",                "originalType" => "string", "model" => json_encode(
            [
                "readonly" => false,
                "value" => "123123123",
                ]),]);
        DB::table("extra_field_types")->insert(["code" => "link", "title" => json_encode(["en" => "Link",              "ru" => "Ссылка", ]),              "icon" => "share-square", "originalType" => "string", "model" => json_encode(
            [
                "readonly" => false,
                "target" => "blank",
                "preview" => false, //TODO: preview link
                "value" => "#",
                ]),]);
        DB::table("extra_field_types")->insert(["code" => "color", "title" => json_encode(["en" => "Color",             "ru" => "Цвет", ]),                "icon" => "palette",                "originalType" => "string", "model" => json_encode(
            [
                "readonly" => false,
                "preset" => ["#000000", "#111111", "#333333",],
                "value" => "#FFFFFF",
                ]),]);

    }
}
