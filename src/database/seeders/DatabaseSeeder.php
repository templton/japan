<?php

namespace Database\Seeders;

use App\Models\Cargo;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            EntitySeeder::class,
            UserSeeder::class,
            VoyageStatusSeeder::class,
            CargoStatusSeeder::class,
            CargoTypeSeeder::class,
            ExportMethodSeeder::class,
            PartnerSeeder::class,
            PortSeeder::class,
            SearchSeeder::class,
            ShipSeeder::class,
            TagSeeder::class,
            VoyageSeeder::class,
            CargoSeeder::class,
        ]);
    }
}
