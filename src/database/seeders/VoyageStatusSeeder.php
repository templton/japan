<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class VoyageStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('voyage_statuses')->insert(['color' => '#0099ff', 'icon' => 'vinyl', 'title' => json_encode(["en" => "draft", "ru" => "черновик"]),]);
        DB::table('voyage_statuses')->insert(['color' => '#ffacac', 'icon' => 'vinyl', 'title' => json_encode(["en" => "wait", "ru" => "сформирован"]),]);
        DB::table('voyage_statuses')->insert(['color' => '#ffacac', 'icon' => 'vinyl', 'title' => json_encode(["en" => "waiting", "ru" => "ожидает"]),]);
        DB::table('voyage_statuses')->insert(['color' => '#ffacac', 'icon' => 'vinyl', 'title' => json_encode(["en" => "loading", "ru" => "погрузка"]),]);
        DB::table('voyage_statuses')->insert(['color' => '#6200ff', 'icon' => 'vinyl', 'title' => json_encode(["en" => "uploaded", "ru" => "погружен"]),]);
        DB::table('voyage_statuses')->insert(['color' => '#0070c6', 'icon' => 'vinyl', 'title' => json_encode(["en" => "on the way", "ru" => "в пути"]),]);
        DB::table('voyage_statuses')->insert(['color' => '#095a41', 'icon' => 'vinyl', 'title' => json_encode(["en" => "arrived", "ru" => "прибыл"]),]);
        DB::table('voyage_statuses')->insert(['color' => '#ffacac', 'icon' => 'vinyl', 'title' => json_encode(["en" => "wait", "ru" => "выгрузка"]),]);
        DB::table('voyage_statuses')->insert(['color' => '#0ab100', 'icon' => 'vinyl', 'title' => json_encode(["en" => "unloaded", "ru" => "выгружен"]),]);
        DB::table('voyage_statuses')->insert(['color' => '#0b4900', 'icon' => 'vinyl', 'title' => json_encode(["en" => "completed", "ru" => "завершен"]),]);
        DB::table('voyage_statuses')->insert(['color' => '#000000', 'icon' => 'vinyl', 'title' => json_encode(["en" => "canceled", "ru" => "отменен"]),]);
        DB::table('voyage_statuses')->insert(['color' => '#c1c1c1', 'icon' => 'vinyl', 'title' => json_encode(["en" => "archived", "ru" => "в архиве"]),]);
    }
}
