<?php

namespace Database\Seeders;

use App\Models\Cargo;
//use Faker\Provider\ru_RU\Person;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CargoSeeder extends Seeder
{
    public function run()
    {
        Cargo::factory()->count(1000)->create();
    }
}


