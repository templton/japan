<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EntitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('entities')->insert(['icon' => 'voyage', 'title' => json_encode(["en" => "Voyage", "ru" => "Рейс"]), 'color' => '#FF7F00', 'url' => '/voyages/',]);
        DB::table('entities')->insert(['icon' => 'cargo', 'title' => json_encode(["en" => "Cargo", "ru" => "Груз"]), 'color' => '#FF7F00', 'url' => '/cargos/',]);
        DB::table('entities')->insert(['icon' => 'user', 'title' => json_encode(["en" => "User", "ru" => "Пользователь"]), 'color' => '#FF7F00', 'url' => '/settings/users/',]);
        DB::table('entities')->insert(['icon' => 'partner', 'title' => json_encode(["en" => "Partner", "ru" => "Партнер"]), 'color' => '#FF7F00', 'url' => '/partners/',]);
        DB::table('entities')->insert(['icon' => 'port', 'title' => json_encode(["en" => "Port", "ru" => "Порт"]), 'color' => '#FF7F00', 'url' => '/settings/ports/',]);
        DB::table('entities')->insert(['icon' => 'ship', 'title' => json_encode(["en" => "Ship", "ru" => "Судно"]), 'color' => '#FF7F00', 'url' => '/settings/ships',]);

    }
}
