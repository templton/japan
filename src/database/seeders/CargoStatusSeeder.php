<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CargoStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cargo_statuses')->insert(['color' => '#0099ff',  'icon' => 'draft', 'title' => json_encode(["en" => "draft", "ru" => "черновик"]),]);
        DB::table('cargo_statuses')->insert(['color' => '#ffacac',  'icon' => 'wait', 'title' => json_encode(["en" => "wait", "ru" => "сформирован"]),]);
        DB::table('cargo_statuses')->insert(['color' => '#ffacac',  'icon' => 'waiting', 'title' => json_encode(["en" => "waiting", "ru" => "ожидает"]),]);
        DB::table('cargo_statuses')->insert(['color' => '#ffacac',  'icon' => 'loading', 'title' => json_encode(["en" => "loading", "ru" => "погрузка"]),]);
        DB::table('cargo_statuses')->insert(['color' => '#6200ff',  'icon' => 'uploaded', 'title' => json_encode(["en" => "uploaded", "ru" => "погружен"]),]);
        DB::table('cargo_statuses')->insert(['color' => '#0070c6',  'icon' => 'onWay', 'title' => json_encode(["en" => "on the way", "ru" => "в пути"]),]);
        DB::table('cargo_statuses')->insert(['color' => '#095a41',  'icon' => 'arrived', 'title' => json_encode(["en" => "arrived", "ru" => "прибыл"]),]);
        DB::table('cargo_statuses')->insert(['color' => '#ffacac',  'icon' => 'waitUpload', 'title' => json_encode(["en" => "wait upload", "ru" => "выгрузка"]),]);
        DB::table('cargo_statuses')->insert(['color' => '#0ab100',  'icon' => 'unloaded', 'title' => json_encode(["en" => "unloaded", "ru" => "выгружен"]),]);
        DB::table('cargo_statuses')->insert(['color' => '#0b4900',  'icon' => 'completed', 'title' => json_encode(["en" => "completed", "ru" => "завершен"]),]);
        DB::table('cargo_statuses')->insert(['color' => '#000000',  'icon' => 'canceled', 'title' => json_encode(["en" => "canceled", "ru" => "отменен"]),]);
        DB::table('cargo_statuses')->insert(['color' => '#c1c1c1',  'icon' => 'archived', 'title' => json_encode(["en" => "archived", "ru" => "в архиве"]),]);

    }
}
