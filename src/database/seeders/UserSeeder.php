<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Sergey Suprunov',
            'email' => 'sergeysuprunov@gmail.com',
            'password' => Hash::make('qazqaz012'),
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
            'photos' => json_encode([["title" => "Sergey Suprunov", "url" => "/img/users/suprunov.jpg",], ]),
            'settings' => json_encode(["language" => "ru",]),
            'permissions' => json_encode(["admin" => true,]),
            'documents' => json_encode([]),
            'tags' => json_encode([1,7,]),
        ]);
        DB::table('users')->insert([
            'name' => 'Yuriy Rayko',
            'email' => 'hi@devcv.ru',
            'password' => Hash::make('qazqaz012'),
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
            'photos' => json_encode([["title" => "No photo", "url" => "/img/users/rayko.jpg",], ]),
            'settings' => json_encode(["language" => "ru",]),
            'permissions' => json_encode(["admin" => true,]),
            'documents' => json_encode([]),
            'tags' => json_encode([1,8,]),
        ]);
        DB::table('users')->insert([
            'name' => 'Pavel Degtyarev',
            'email' => 'zapros@mail.ru',
            'password' => Hash::make('qazqaz012'),
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),
            'photos' => json_encode([["title" => "No photo", "url" => "/img/placeholder/640x640.jpg",], ]),
            'settings' => json_encode(["language" => "en",]),
            'permissions' => json_encode(["admin" => false,]),
            'documents' => json_encode([]),
            'tags' => json_encode([1,9,]),
        ]);
    }
}
