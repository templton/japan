<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShipSeeder extends Seeder
{
    /*
                 $table->id();
                $table->string('title')->default("Ship title in English");
                $table->json('country')->default(json_encode(["en" => "Japan", "ru" => "Япония", "flag" => "jp",]));
                $table->text('description')->nullable();
                $table->json('tags')->default(json_encode([]));
                $table->json('photos')->default(json_encode([["title" => "No photo", "url" => "/img/placeholder/640x480.jpg"],]));
                $table->json('extraFields')->default(json_encode(
                    [
                        "link" => ["title" => ["en" => "Ship on marinetraffic.com", "ru" => "Судно на marinetraffic.com"], "type" => "link", "required" => false, "value" => null],
                        "owner" => ["title" => ["en" => "Owner", "ru" => "Владелец"], "type" => "string", "required" => true, "value" => "owner"],
                        "captain" => ["title" => ["en" => "Captain", "ru" => "Капитан"], "type" => "string", "required" => true, "value" => "captain"],
                        "imo" => ["title" => ["en" => "IMO", "ru" => "IMO"], "type" => "string", "required" => false, "value" => null],
                        "coords" => ["title" => ["en" => "Coordinates", "ru" => "Координаты"], "type" => "coordinates", "required" => false, "value" => null],
                        "width" => ["title" => ["en" => "Width", "ru" => "Ширина"], "type" => "number", "required" => true, "value" => 0],
                        "length" => ["title" => ["en" => "Length", "ru" => "Длина"], "type" => "number", "required" => true, "value" => 0],
                        "height" => ["title" => ["en" => "Height", "ru" => "Высота"], "type" => "number", "required" => true, "value" => 0],
                        "grossTonnage" => ["title" => ["en" => "Gross tonnage", "ru" => "Вместимость судна"], "type" => "number", "required" => true, "value" => 0],
                        "summerDWT" => ["title" => ["en" => "Summer DWT", "ru" => "Летний дедвейт"], "type" => "number", "required" => true, "value" => 0],
                        "places" => ["title" => ["en" => "Places count", "ru" => "Количество мест"], "type" => "number", "required" => true, "value" => 0],
                    ]));
                $table->json('documents')->default(json_encode([]));
     */
    public function run()
    {
        DB::table('ships')->insert([
            'title' => 'Racer',
            'country' => json_encode(['en' => 'Togo', 'ru' => 'Того', 'flag' => 'tg']),

            'photos' => json_encode([["title" => "Racer", "url" => "racer-1.jpg"], ["title" => "Racer", "url" => "racer-2.jpg",],]),
            'extraFields' => json_encode(["owner" => ["title" => ["en" => "Owner", "ru" => "Владелец"], "type" => "string", "required" => false, "value" => "AtlasPacificShipping Co,LTD"], "captain" => ["title" => ["en" => "Captain", "ru" => "Капитан"], "type" => "string", "required" => false, "value" => "Mr. Smith"], "imo" => ["title" => ["en" => "IMO", "ru" => "IMO"], "type" => "string", "required" => false, "value" => "8717659"], "coords" => ["title" => ["en" => "Coordinates", "ru" => "Координаты"], "type" => "coordinates", "required" => false, "value" => ["lat" => 40.38143, "lng" => 134.7258]], "width" => ["title" => ["en" => "Width", "ru" => "Ширина"], "type" => "number", "required" => true, "value" => 11.02], "length" => ["title" => ["en" => "Length", "ru" => "Длина"], "type" => "number", "required" => true, "value" => 77.66], "height" => ["title" => ["en" => "Height", "ru" => "Высота"], "type" => "number", "required" => true, "value" => 12], "grossTonnage" => ["title" => ["en" => "Gross tonnage", "ru" => "Вместимость судна"], "type" => "number", "required" => true, "value" => 1116], "summerDWT" => ["title" => ["en" => "Summer DWT", "ru" => "Летний дедвейт"], "type" => "number", "required" => true, "value" => 871], "places" => ["title" => ["en" => "Places count", "ru" => "Количество мест"], "type" => "number", "required" => true, "value" => 12],]),
            'documents' => json_encode([]),'tags' => json_encode([6,])]);
        DB::table('ships')->insert([
            'title' => "Ocean pride 1",
            'country' => json_encode(["Sierra Leone", "ru" => "Сьерра-Леоне", "flag" => "sl"]),

            'photos' => json_encode([["title" => "Ocean pride 1", "url" => "ocean-pride-1-1.jpg",], ["title" => "Ocean pride 1", "url" => "ocean-pride-1-2.jpg",],]),
            'extraFields' => json_encode(["owner" => ["title" => ["en" => "Owner", "ru" => "Владелец"], "type" => "string", "required" => false, "value" => "AtlasPacificShipping Co,LTD"], "captain" => ["title" => ["en" => "Captain", "ru" => "Капитан"], "type" => "string", "required" => false, "value" => "John Doe",], "imo" => ["title" => ["en" => "IMO", "ru" => "IMO"], "type" => "string", "required" => false, "value" => "9205873"], "coords" => ["title" => ["en" => "Coordinates", "ru" => "Координаты"], "type" => "coordinates", "required" => false, "value" => ["lat" => 40.38143, "lng" => 134.7258]], "width" => ["title" => ["en" => "Width", "ru" => "Ширина"], "type" => "number", "required" => true, "value" => 42], "length" => ["title" => ["en" => "Length", "ru" => "Длина"], "type" => "number", "required" => true, "value" => 240.99], "height" => ["title" => ["en" => "Height", "ru" => "Высота"], "type" => "number", "required" => true, "value" => 12], "grossTonnage" => ["title" => ["en" => "Gross tonnage", "ru" => "Вместимость судна"], "type" => "number", "required" => true, "value" => 56693,], "summerDWT" => ["title" => ["en" => "Summer DWT", "ru" => "Летний дедвейт"], "type" => "number", "required" => true, "value" => 105715], "places" => ["title" => ["en" => "Places count", "ru" => "Количество мест"], "type" => "number", "required" => true, "value" => 12],]),
            'documents' => json_encode([]),'tags' => json_encode([6,])]);
        DB::table('ships')->insert([
            'title' => "Safe",
            'country' => json_encode(["Togo", "ru" => "Того", "flag" => "tg"]),

        'photos' => json_encode([["title" => "Safe", "url" => "safe-1.jpg",], ["title" => "Safe", "url" => "safe-2.jpg",],]),
            'extraFields' => json_encode(["owner" => ["title" => ["en" => "Owner", "ru" => "Владелец"], "type" => "string", "required" => false, "value" => "SAKURA SHIPPING LTD"], "captain" => ["title" => ["en" => "Captain", "ru" => "Капитан"], "type" => "string", "required" => false, "value" => "Jack Sparrow",], "imo" => ["title" => ["en" => "IMO", "ru" => "IMO"], "type" => "string", "required" => false, "value" => "8898386"], "coords" => ["title" => ["en" => "Coordinates", "ru" => "Координаты"], "type" => "coordinates", "required" => false, "value" => ["lat" => 40.38143, "lng" => 134.7258]], "width" => ["title" => ["en" => "Width", "ru" => "Ширина"], "type" => "number", "required" => true, "value" => 13], "length" => ["title" => ["en" => "Length", "ru" => "Длина"], "type" => "number", "required" => true, "value" => 74], "height" => ["title" => ["en" => "Height", "ru" => "Высота"], "type" => "number", "required" => true, "value" => 12], "grossTonnage" => ["title" => ["en" => "Gross tonnage", "ru" => "Вместимость судна"], "type" => "number", "required" => true, "value" => 1528,], "summerDWT" => ["title" => ["en" => "Summer DWT", "ru" => "Летний дедвейт"], "type" => "number", "required" => true, "value" => 2419], "places" => ["title" => ["en" => "Places count", "ru" => "Количество мест"], "type" => "number", "required" => true, "value" => 12],]),
            'documents' => json_encode([]),]);
        DB::table('ships')->insert([
            'title' => "Sun Rio",
            'country' => json_encode(["Panama", "ru" => "Панама", "flag" => "pa"]),

            'photos' => json_encode([["title" => "Sun Rio", "url" => "sun-rio-1.jpg",], ["title" => "Sun Rio", "url" => "sun-rio-2.jpg",],]),
            'extraFields' => json_encode(["owner" => ["title" => ["en" => "Owner", "ru" => "Владелец"], "type" => "string", "required" => false, "value" => "SAKURA SHIPPING LTD"], "captain" => ["title" => ["en" => "Captain", "ru" => "Капитан"], "type" => "string", "required" => false, "value" => "Mr. Smith",], "imo" => ["title" => ["en" => "IMO", "ru" => "IMO"], "type" => "string", "required" => false, "value" => "9030644"], "coords" => ["title" => ["en" => "Coordinates", "ru" => "Координаты"], "type" => "coordinates", "required" => false, "value" => ["lat" => 40.38143, "lng" => 134.7258]], "width" => ["title" => ["en" => "Width", "ru" => "Ширина"], "type" => "number", "required" => true, "value" => 16], "length" => ["title" => ["en" => "Length", "ru" => "Длина"], "type" => "number", "required" => true, "value" => 131.16], "height" => ["title" => ["en" => "Height", "ru" => "Высота"], "type" => "number", "required" => true, "value" => 12], "grossTonnage" => ["title" => ["en" => "Gross tonnage", "ru" => "Вместимость судна"], "type" => "number", "required" => true, "value" => 7578,], "summerDWT" => ["title" => ["en" => "Summer DWT", "ru" => "Летний дедвейт"], "type" => "number", "required" => true, "value" => 2577], "places" => ["title" => ["en" => "Places count", "ru" => "Количество мест"], "type" => "number", "required" => true, "value" => 12],]),
            'documents' => json_encode([]),'tags' => json_encode([6,])]);
        DB::table('ships')->insert([
            'title' => "Silver Dream",
            'country' => json_encode(["Sierra Leone", "ru" => "Сьерра-Леоне", "flag" => "sl"]),

            'photos' => json_encode([["title" => "Silver Dream", "url" => "silver-dream-1.jpg",], ["title" => "Silver Dream", "url" => "silver-dream-2.jpg",], ["title" => "Silver Dream", "url" => "silver-dream-3.jpg",], ["title" => "Silver Dream", "url" => "silver-dream-4.jpg",],]),
            'extraFields' => json_encode(["owner" => ["title" => ["en" => "Owner", "ru" => "Владелец"], "type" => "string", "required" => false, "value" => "SAKURA SHIPPING LTD"], "captain" => ["title" => ["en" => "Captain", "ru" => "Капитан"], "type" => "string", "required" => false, "value" => "Mr. Smith",], "imo" => ["title" => ["en" => "IMO", "ru" => "IMO"], "type" => "string", "required" => false, "value" => "9359222"], "coords" => ["title" => ["en" => "Coordinates", "ru" => "Координаты"], "type" => "coordinates", "required" => false, "value" => ["lat" => 40.38143, "lng" => 134.7258]], "width" => ["title" => ["en" => "Width", "ru" => "Ширина"], "type" => "number", "required" => true, "value" => 13], "length" => ["title" => ["en" => "Length", "ru" => "Длина"], "type" => "number", "required" => true, "value" => 83], "height" => ["title" => ["en" => "Height", "ru" => "Высота"], "type" => "number", "required" => true, "value" => 12], "grossTonnage" => ["title" => ["en" => "Gross tonnage", "ru" => "Вместимость судна"], "type" => "number", "required" => true, "value" => 2039,], "summerDWT" => ["title" => ["en" => "Summer DWT", "ru" => "Летний дедвейт"], "type" => "number", "required" => true, "value" => 2987], "places" => ["title" => ["en" => "Places count", "ru" => "Количество мест"], "type" => "number", "required" => true, "value" => 12],]),
            'documents' => json_encode([]),'tags' => json_encode([6,])]);
        DB::table('ships')->insert([
            'title' => "Silver way",
            'country' => json_encode(["Sierra Leone", "ru" => "Сьерра-Леоне", "flag" => "sl"]),

            'photos' => json_encode([["title" => "Silver way", "url" => "silver-way-1.jpg",], ["title" => "Silver way", "url" => "silver-way-2.jpg",],]),
            'extraFields' => json_encode(["owner" => ["title" => ["en" => "Owner", "ru" => "Владелец"], "type" => "string", "required" => false, "value" => "SAKURA SHIPPING LTD"], "captain" => ["title" => ["en" => "Captain", "ru" => "Капитан"], "type" => "string", "required" => false, "value" => "Mr. Smith",], "imo" => ["title" => ["en" => "IMO", "ru" => "IMO"], "type" => "string", "required" => false, "value" => "8858855"], "coords" => ["title" => ["en" => "Coordinates", "ru" => "Координаты"], "type" => "coordinates", "required" => false, "value" => ["lat" => 40.38143, "lng" => 134.7258]], "width" => ["title" => ["en" => "Width", "ru" => "Ширина"], "type" => "number", "required" => true, "value" => 15], "length" => ["title" => ["en" => "Length", "ru" => "Длина"], "type" => "number", "required" => true, "value" => 85.7], "height" => ["title" => ["en" => "Height", "ru" => "Высота"], "type" => "number", "required" => true, "value" => 12], "grossTonnage" => ["title" => ["en" => "Gross tonnage", "ru" => "Вместимость судна"], "type" => "number", "required" => true, "value" => 2299,], "summerDWT" => ["title" => ["en" => "Summer DWT", "ru" => "Летний дедвейт"], "type" => "number", "required" => true, "value" => 3501], "places" => ["title" => ["en" => "Places count", "ru" => "Количество мест"], "type" => "number", "required" => true, "value" => 12],]),
            'documents' => json_encode([]),'tags' => json_encode([6,])]);
    }
}
