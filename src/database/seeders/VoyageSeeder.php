<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VoyageSeeder extends Seeder
{
    public function run()
    {
        DB::table('voyages')->insert(['created_at' => '2020-10-01 12:12:12',  'updated_at' => '2020-10-07 12:12:12',  'title' => 'V00001', 'ownerUserId' => rand(1,3),   'srcPortId' => rand(1,5),   'departDate' => '2020-10-01',  'destPortId' => rand(6,7),  'arrivalDate' => '2020-10-07', 'statusId' => rand(1, 12),  'shipId' => rand(1,6),  'description' => 'bla-bla-bla...1', 'tags' => json_encode([1,]),]);
        DB::table('voyages')->insert(['created_at' => '2020-10-02 12:12:12',  'updated_at' => '2020-10-08 12:12:12',  'title' => 'V00002', 'ownerUserId' => rand(1,3),   'srcPortId' => rand(1,5),   'departDate' => '2020-10-02',  'destPortId' => rand(6,7),  'arrivalDate' => '2020-10-08', 'statusId' => rand(1, 12),  'shipId' => rand(1,6),  'description' => 'bla-bla-bla...2', 'tags' => json_encode([1,]),]);
        DB::table('voyages')->insert(['created_at' => '2020-10-03 12:12:12',  'updated_at' => '2020-10-09 12:12:12',  'title' => 'V00003', 'ownerUserId' => rand(1,3),   'srcPortId' => rand(1,5),   'departDate' => '2020-10-03',  'destPortId' => rand(6,7),  'arrivalDate' => '2020-10-09', 'statusId' => rand(1, 12),  'shipId' => rand(1,6),  'description' => 'bla-bla-bla...3', 'tags' => json_encode([1,]),]);
        DB::table('voyages')->insert(['created_at' => '2020-10-04 12:12:12',  'updated_at' => '2020-10-10 12:12:12',  'title' => 'V00004', 'ownerUserId' => rand(1,3),   'srcPortId' => rand(1,5),   'departDate' => '2020-10-04',  'destPortId' => rand(6,7),  'arrivalDate' => '2020-10-10', 'statusId' => rand(1, 12),  'shipId' => rand(1,6),  'description' => 'bla-bla-bla...4', 'tags' => json_encode([1,]),]);
        DB::table('voyages')->insert(['created_at' => '2020-10-05 12:12:12',  'updated_at' => '2020-10-11 12:12:12',  'title' => 'V00005', 'ownerUserId' => rand(1,3),   'srcPortId' => rand(1,5),   'departDate' => '2020-10-05',  'destPortId' => rand(6,7),  'arrivalDate' => '2020-10-11', 'statusId' => rand(1, 12),  'shipId' => rand(1,6),  'description' => 'bla-bla-bla...5',  'tags' => json_encode([1,]),]);
        DB::table('voyages')->insert(['created_at' => '2020-10-06 12:12:12',  'updated_at' => '2020-10-12 12:12:12',  'title' => 'V00006', 'ownerUserId' => rand(1,3),   'srcPortId' => rand(1,5),   'departDate' => '2020-10-06',  'destPortId' => rand(6,7),  'arrivalDate' => '2020-10-12', 'statusId' => rand(1, 12),  'shipId' => rand(1,6),  'description' => 'bla-bla-bla...6',  'tags' => json_encode([1,]),]);
    }
}
