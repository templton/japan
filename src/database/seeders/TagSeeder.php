<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagSeeder extends Seeder
{
    public function run()
    {
        DB::table('tags')->insert(['title'=> "Fake", 'color' => '#895020', 'icon' => 'fake',]);
        DB::table('tags')->insert(['title'=> "Customer", 'color' => '#0055FF', 'icon' => 'person',]);
        DB::table('tags')->insert(['title'=> "Sender", 'color' => '#4EA654', 'icon' => 'person',]);
        DB::table('tags')->insert(['title'=> "Regular customer", 'color' => '#4E75A7', 'icon' => 'like',]);
        DB::table('tags')->insert(['title'=> "Russia", 'color' => '#409999', 'icon' => 'star',]);
        DB::table('tags')->insert(['title'=> "Japan", 'color' => '#FF9E6B', 'icon' => 'star',]);
        DB::table('tags')->insert(['title'=> "Admin", 'color' => '#FF0000', 'icon' => 'user-cog',]);
        DB::table('tags')->insert(['title'=> "User", 'color' => '#8157BA', 'icon' => 'user-check',]);
        DB::table('tags')->insert(['title'=> "Temporary user", 'color' => '#888888', 'icon' => 'user-clock',]);
    }
}
