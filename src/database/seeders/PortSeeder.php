<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PortSeeder extends Seeder
{
    public function run()
    {
        DB::table('ports')->insert(['title' => json_encode(["en" => "Fushikitoyama", "ru" => "Фусики Тояма"]), 'extraFields' => json_encode(['coords' => ["lat" => 36.78536, "lng" => 137.09165]]), 'country' => json_encode(["en" => "Japan", "ru" => "Япония", "flag" => "jp"]), 'tags' => json_encode([6,])]);
        DB::table('ports')->insert(['title' => json_encode(["en" => "Fukui", "ru" => "Фукуи"]), 'extraFields' => json_encode(['coords' => ["lat" => 36.2037, "lng" => 136.1326]]), 'country' => json_encode(["en" => "Japan", "ru" => "Япония", "flag" => "jp"]),'tags' => json_encode([6,])]);
        DB::table('ports')->insert(['title' => json_encode(["en" => "Tsuruga", "ru" => "Цуруга"]), 'extraFields' => json_encode(['coords' => ["lat" => 35.674175, "lng" => 136.0567]]), 'country' => json_encode(["en" => "Japan", "ru" => "Япония", "flag" => "jp"]),'tags' => json_encode([6,])]);
        DB::table('ports')->insert(['title' => json_encode(["en" => "Niigata", "ru" => "Ниигата"]), 'extraFields' => json_encode(['coords' => ["lat" => 37.977155, "lng" => 139.1701]]), 'country' => json_encode(["en" => "Japan", "ru" => "Япония", "flag" => "jp"]),'tags' => json_encode([6,])]);
        DB::table('ports')->insert(['title' => json_encode(["en" => "Hakata", "ru" => "Хаката"]), 'extraFields' => json_encode(['coords' => ["lat" => 33.62869, "lng" => 130.37655]]), 'country' => json_encode(["en" => "Japan", "ru" => "Япония", "flag" => "jp"]),'tags' => json_encode([6,])]);
        DB::table('ports')->insert(['title' => json_encode(["en" => "Vladivostok", "ru" => "Владивосток"]), 'extraFields' => json_encode(['coords' => ["lat" => 43.087445, "lng" => 131.9022]]), 'country' => json_encode(["en" => "Russia", "ru" => "Россия", "flag" => "ru"]),'tags' => json_encode([5,])]);
        DB::table('ports')->insert(['title' => json_encode(["en" => "Novorossiysk", "ru" => "Новороссийск"]), 'extraFields' => json_encode(['coords' => ["lat" => 44.720065, "lng" => 37.81373]]), 'country' => json_encode(["en" => "Russia", "ru" => "Россия", "flag" => "ru"]),'tags' => json_encode([5,])]);

    }
}
