<?php

namespace App\GraphQL\Mutations;

use App\Models\User;

class UpdateUser
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        $user = User::find($args['id']);

        if(isset($args['file'])) {
            $file = $args['file'];
            \Storage::put('uploads', $file);
            $user->photo = $file->hashName();
        }

        if(isset($args['name'])) {
            $user->name = $args['name'];
        }
        if(isset($args['email'])) {
            $user->email = $args['email'];
        }
        if(isset($args['password'])) {
            $user->password = \Hash::make($args['password']);
        }
        $user->save();
        return $user;
    }
}
