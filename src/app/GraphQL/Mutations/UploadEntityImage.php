<?php

namespace App\GraphQL\Mutations;

use App\Models\Cargo;
use App\Repository\CargoRepository;
use App\Repository\ShipRepository;
use App\Models\ship;

class UploadEntityImage
{

    const ENTITY_CARGO = 'cargo';
    const ENTITY_SHIP = 'ship';

    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        $file = $args['input']['file'];
        \Storage::put('public', $file);
        $hash = $file->hashName();
        $url = 'http://amixline.loc/storage/' . $hash;

        $entityId = $args['input']['entityId'];

        switch ($args['input']['entityName']){
            case self::ENTITY_CARGO:
                $model = Cargo::find($entityId);
                $cargoRepository = new CargoRepository();
                $model = $cargoRepository->addPhoto($model, $url, 'NEW PHOTO TITLE');
                break;
            case self::ENTITY_SHIP:
                $model = Ship::find($entityId);
                $shipRepository = new ShipRepository();
                $model = $shipRepository->addPhoto($model, $url, 'NEW PHOTO TITLE');
                break;
            default:
                throw new \Exception('Entity name is required');
        }

        $response = new \stdClass();
        $response->url = $url;
        $response->hash = $hash;

        return $response;
    }
}
