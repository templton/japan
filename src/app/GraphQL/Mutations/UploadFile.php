<?php

namespace App\GraphQL\Mutations;

class UploadFile
{
    /**
     * @param  null  $_
     * @param  array<string, mixed>  $args
     */
    public function __invoke($_, array $args)
    {
        $file = $args['file'];

        \Storage::put('uploads', $file);
        $hash = $file->hashName();

        return $file->storePublicly('uploads');
    }
}
