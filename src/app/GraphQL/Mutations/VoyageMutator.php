<?php

namespace App\GraphQL\Mutations;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Models\Cargo;
use phpDocumentor\Reflection\Types\Context;
use App\Models\User;
use App\Models\Voyage;

class VoyageMutator
{

    public function create($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): array
    {
        $model = new Voyage();
        $model = $this->fillModelWithArgs($model, $args);
        //$model->departDate = date('Y-m-d');
        //$model->arrivalDate = date('Y-m-d');
        $model->save();

        return [
            'data' => $model,
            'error' => null
        ];
    }

    public function update($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo): array
    {
        $model = Voyage::find($args['id']);
        $model = $this->fillModelWithArgs($model, $args);
        $model->save();

        return [
            'data' => $model,
            'error' => null
        ];
    }

    private function fillModelWithArgs(Voyage $model, array $args): Voyage
    {
        $fields = $model->getFillable();

        foreach ($fields as $field) {
            if (isset($args[$field])){
                $model->{$field} = $args[$field];
            }
        }

        return $model;
    }
}
