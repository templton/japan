<?php

namespace App\GraphQL\Mutations;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use GraphQL\Type\Definition\ResolveInfo;
use Nuwave\Lighthouse\Support\Contracts\GraphQLContext;
use App\Models\Cargo;
use phpDocumentor\Reflection\Types\Context;
use App\Models\User;


class CargoMutator
{
    public function create($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {

        $cargo = new Cargo($args);
        $cargo = $this->mapArgsToModel($cargo, $args);
        $cargo->ownerUserId = $context->user()->getAuthIdentifier();
        $cargo->createdById = $context->user()->getAuthIdentifier();
        $cargo->save();

        return [
            'data' => $cargo,
            'error' => null
        ];
    }

    public function update($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo)
    {
        $cargo = Cargo::find($args['id']);
        $cargo = $this->mapArgsToModel($cargo, $args);
        $cargo->voyageId = $args['voyageId'];
        $cargo->save();

        return [
            'data' => Cargo::find($args['id']),
            'error' => null
        ];
    }

    public function updateAny($rootValue, array $args, GraphQLContext $context, ResolveInfo $resolveInfo){
        $cargo = Cargo::find($args['id']);
        $cargo = $this->fillModelWithArgs($cargo, $args);
        $cargo->save();

        return [
            'data' => $cargo,
            'error' => null
        ];
    }

    private function fillModelWithArgs(Cargo $model, array $args): Cargo
    {
        $fields = $model->getFillable();

        foreach ($fields as $field) {
            if (isset($args[$field])){
                $model->{$field} = $args[$field];
            }
        }

        return $model;
    }

    private function mapArgsToModel($cargo, $args)
    {
        $cargo->title = $args['title'];
        $cargo->senderPartnerId = $args['senderPartnerId'];
        $cargo->clientPartnerId = $args['clientPartnerId'];
        $cargo->receiverPartnerId = $args['receiverPartnerId'];
        $cargo->customsPartnerId = $args['customsPartnerId'];
        $cargo->exportMethodId = $args['exportMethodId'];
        $cargo->cargoTypeId = $args['cargoTypeId'];
        $cargo->extraFields = $args['extraFields'];
        $cargo->parentId = $args['parentId'] ?? null;

        if (isset($args['statusId'])){
            $cargo->statusId = $args['statusId'] ?? null;
        }

        return $cargo;
    }
}
