<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class search extends Model
{
    use HasFactory;
    public function entity(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Entity::class, 'id', 'entityTypeId');
    }

    public function value(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Entity::class, 'id', 'entityTypeId');
//        TODO: switch
    }


}
