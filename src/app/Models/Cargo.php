<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'extraFields',
        'statusId',
        'exportMethodId',
        'cargoTypeId',
        'ownerUserId',
        'senderPartnerId',
        'clientPartnerId',
        'receiverPartnerId',
        'customsPartnerId',
        'voyageId',
        'parentId'
    ];

    public function createdBy(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'createdById');
    }

    public function updatedBy(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'updatedById');
    }

    public function owner(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'ownerUserId');
    }

    public function status(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(CargoStatus::class, 'id', 'statusId');
    }

    public function exportMethod(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(ExportMethod::class, 'id', 'exportMethodId');
    }

    public function cargoType(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(CargoType::class, 'id', 'cargoTypeId');
    }

    public function voyage(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Voyage::class, 'id', 'voyageId');
    }

    public function sender(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Partner::class, 'id', 'senderPartnerId');
    }

    public function client(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Partner::class, 'id', 'clientPartnerId');
    }

    public function receiver(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Partner::class, 'id', 'receiverPartnerId');
    }

    public function customs(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Partner::class, 'id', 'customsPartnerId');
    }

    public function parent(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(self::class, 'id', 'parentId');
    }
}
