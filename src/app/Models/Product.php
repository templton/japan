<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;


class Product extends Model
{
    public function upsales(){
        return $this->hasMany(ProductUpsale::class, 'product_id', 'id');
    }
    public function group() {
        return $this->hasOne(ProductGroup::class, 'id', 'product_group_id');
    }
    public function tags() {
        return $this->belongsToMany(Tag::class);
    }

    public function properties() {
        return $this->belongsToMany(PropertyValue::class);
    }

}
