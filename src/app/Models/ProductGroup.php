<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductGroup extends Model
{
    public function products()
    {
        return $this->hasMany(Product::class, 'product_group_id', 'id');
    }
}
