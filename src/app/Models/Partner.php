<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    use HasFactory;
    public function createdBy(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'createdById');
    }
    public function updatedBy(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'updatedById');
    }
    public function owner(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'ownerUserId');
    }
    public function sender(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Cargo::class, 'senderPartnerId', 'id');
    }
    public function client(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Cargo::class, 'clientPartnerId', 'id');
    }
    public function receiver(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Cargo::class, 'receiverPartnerId', 'id');
    }
    public function customs(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Cargo::class, 'customsPartnerId', 'id');
    }
}
