<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class port extends Model
{
    use HasFactory;

    public function createdBy(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'createdById');
    }
    public function updatedBy(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'updatedById');
    }
    public function voyages(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Voyage::class);
    }

    public function sourceInVoyages(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Voyage::class, 'srcPortId', 'id');
    }
    public function destinationInVoyages(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Voyage::class, 'destPortId', 'id');
    }

}
