<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class cargoStatus extends Model
{
    public function createdBy(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'createdById');
    }
    public function updatedBy(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'updatedById');
    }
    public function cargos(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Cargo::class, 'statusId', 'id');
    }
}
