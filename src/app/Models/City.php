<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class City extends Model
{
    public function units()
    {
        return $this->hasMany(Unit::class, 'city_id', 'id');
    }
    public function country() {
        return $this->belongsTo(Country::class);
    }
}
