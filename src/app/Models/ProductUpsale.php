<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class ProductUpsale extends Model
{
    public function product()
    {
//        $products = Product::all(['id', 'title', 'image'])->where('id', $this->upsale_product_id)->get();
        return $this->hasOne(Product::class, 'id', 'upsale_product_id');
//        return Product::all(['id', 'title', 'image'])->where('id', $this->upsale_product_id)->get();

    }
}
