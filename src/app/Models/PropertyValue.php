<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PropertyValue extends Model
{
    public function property() {
        return $this->hasOne(Property::class, 'id', 'property_id');
    }

    public function products() {
        return $this->belongsToMany(Product::class);
    }
}
