<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use AshAllenDesign\LaravelExchangeRates\Classes\ExchangeRate;
use AshAllenDesign\LaravelExchangeRates\Exceptions\ExchangeRateException;
use AshAllenDesign\LaravelExchangeRates\Exceptions\InvalidCurrencyException;
use AshAllenDesign\LaravelExchangeRates\Exceptions\InvalidDateException;
use Illuminate\Support\Carbon;


class Exchange extends Model
{

    public function rate_for_date(Carbon $date)
    {
        $rate = new ExchangeRate();
        try {
            return $rate->exchangeRate('EUR', 'USD', $date);
        } catch (ExchangeRateException $e) {
        } catch (InvalidCurrencyException $e) {
        } catch (InvalidDateException $e) {
        }
    }
}
