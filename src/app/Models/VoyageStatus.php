<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class voyageStatus extends Model
{
    public function createdBy(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'createdById');
    }
    public function updatedBy(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'updatedById');
    }
    public function voyages(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Voyage::class, 'statusId', 'id');
    }
}
