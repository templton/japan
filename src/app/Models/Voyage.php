<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voyage extends Model
{

    protected $fillable = [
        'title',
        'shipId',
        'srcPortId',
        'destPortId',
        'departDate',
        'arrivalDate',
    ];

    public function status(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(voyageStatus::class, 'id', 'statusId');
    }

    public function createdBy(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'createdById');
    }
    public function updatedBy(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'updatedById');
    }
    public function owner(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(User::class, 'id', 'ownerUserId');
    }

    public function ship(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Ship::class, 'id', 'shipId');
    }

    public function srcPort(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Port::class, 'id', 'srcPortId');
    }

    public function destPort(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(Port::class, 'id', 'destPortId');
    }

    public function cargos(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Cargo::class, 'voyageId', 'id');
    }
}
