<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    public function type() {
        return $this->hasOne(PropertyType::class, 'id', 'property_type_id');
    }
    public function values(){
        return $this->hasMany(PropertyValue::class, 'property_id', 'id');
    }
}
