<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{
    use HasFactory;
    public function searches(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Search::class, 'entityTypeId', 'id');
    }
}

