<?php

namespace App\Repository;

use App\Models\ship;

class ShipRepository
{
    public function addPhoto(ship $ship, string $url, string $title): ship
    {
        $photos = $ship->photos;

        if (!empty($photos)){
            $photos = json_decode($photos);
        }

        if (empty($photos)){
            $photos = [];
        }

        $photos[] = [
            'url' => $url,
            'title' => $title
        ];

        $ship->photos = json_encode($photos, JSON_UNESCAPED_UNICODE);
        $ship->save();

        return $ship;
    }
}
