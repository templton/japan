<?php

namespace App\Repository;

use App\Models\Cargo;

class CargoRepository
{
    public function addPhoto(Cargo $cargo, string $url, string $title): Cargo
    {
        $photos = $cargo->photos;

        if (!empty($photos)){
            $photos = json_decode($photos);
        }

        if (empty($photos)){
            $photos = [];
        }

        $photos[] = [
            'url' => $url,
            'title' => $title
        ];

        $cargo->photos = json_encode($photos, JSON_UNESCAPED_UNICODE);
        $cargo->save();

        return $cargo;
    }
}
