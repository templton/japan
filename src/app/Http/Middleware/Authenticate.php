<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use App\Models\User;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        if (!$request->expectsJson()) {
            return route('login');
        }
    }

    public function handle($request, \Closure $next)
    {
//        $token = $request->bearerToken();
//        $user = \App\User::where('api_token', $token)->first();

        $userId = $request->get('userId');

        $user = User::find($userId);

        if ($user){
            auth()->login($user);
            return $next($request);
        }

        return response([
            'message' => 'Unauthenticated'
        ], 403);
    }
}
