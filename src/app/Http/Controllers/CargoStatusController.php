<?php

namespace App\Http\Controllers;

use App\Models\cargoStatus;
use Illuminate\Http\Request;

class CargoStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\cargoStatus  $cargoStatus
     * @return \Illuminate\Http\Response
     */
    public function show(cargoStatus $cargoStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\cargoStatus  $cargoStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(cargoStatus $cargoStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\cargoStatus  $cargoStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, cargoStatus $cargoStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\cargoStatus  $cargoStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(cargoStatus $cargoStatus)
    {
        //
    }
}
