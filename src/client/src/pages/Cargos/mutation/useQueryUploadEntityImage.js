import { useMutation } from "@apollo/react-hooks";
import {loader} from 'graphql.macro';

const query = loader('./GraphQL/queryUploadEntityImage.graphql');

export const useQueryUploadEntityImage = () => {
    const [uploadEntityImage, {loading}] = useMutation(query)
    return {uploadEntityImage, loading};
}
