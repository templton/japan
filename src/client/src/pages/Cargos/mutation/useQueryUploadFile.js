import { useMutation } from "@apollo/react-hooks";
import {loader} from 'graphql.macro';

const query = loader('./GraphQL/queryUploadFile.graphql');

export const useQueryUploadFile = () => {
    const [uploadFile, {loading}] = useMutation(query)
    return {uploadFile, loading};
}
