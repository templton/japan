import React, { useEffect } from 'react';
import {Gallery} from "../../../../components/Gallery";
import {Button} from "material-ui-bootstrap";
import {Modal} from "@material-ui/core";
import {StyleUploader} from "../../../../components/ImageUploader/ImageUploader";
import {useQueryUploadFile} from "../../mutation/useQueryUploadFile";
import {useQueryUploadEntityImage} from "../../mutation/useQueryUploadEntityImage";

export const PhotosTab = (props) => {
    const { initPhotos, entityName, entityId } = props;
    const [images, setImages] = React.useState([]);
    const [openModal, setOpenModal] = React.useState(false);
    const {uploadEntityImage, loading} = useQueryUploadEntityImage();

    const modalCloseHandler = () => {
        setOpenModal(false);
    }

    const galleryItems = images.map(item => {
        return {
            url: item.data_url,
            title: "new"
        }
    });

    if (initPhotos && initPhotos.length){
        initPhotos.map(item => {
            galleryItems.push(item);
        });
    }

    const renderItem = (item) => {
        return (
            <div>
                <div>
                    {/*<Button color="primary" variant="contained">Удалить</Button>*/}
                </div>
                <div>
                    <img src={item.url} alt={item.title} title={item.title} height={300} />
                </div>
            </div>
        )
    }

    const addPhotoButtonClick = () => {
        setOpenModal(true);
    }

    const storeFile = async (image) => {

        if (!entityId){
            alert("При добавление нового груза пока что нельзя сохранить фото(( Файл отобразится в карусели, но сохранен не будет. Только при редактировании существующего груза. Но скоро будет можно!");
            return;
        }

        if (image.file.size > 1999000){
            alert("Пока что нельзя добавить файл больше 2 мегабайт(( Файл отобразится в карусели, но сохранен не будет. Но скоро будет можно!");
            return;
        }

        const file = image.file;
        const response = await uploadEntityImage({
            variables: {
                file,
                entityName,
                entityId
            }
        });

        console.log(response.data.uploadFile);
    }

    return (
        <div>
            <Button color="primary" onClick={addPhotoButtonClick}>Edit photos list</Button>
            <Gallery items={galleryItems} renderItem={renderItem}/>

            <Modal
                open={openModal}
                onClose={modalCloseHandler}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
            >
                <>
                    <StyleUploader images={images} setImages={setImages} storeFile={storeFile}/>
                </>
            </Modal>
        </div>
    );
}
