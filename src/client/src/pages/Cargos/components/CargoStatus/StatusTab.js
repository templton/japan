import React, {useContext} from "react";
import {Form, TextField} from "material-ui-pack"
import {PropTypes} from "prop-types";
import { StatusText } from "../../../../components/Status";
import {StatusSelect} from "../../../../components/Status/StatusSelect";
import {DictionaryContext} from "../../../../components/DictionaryContext/DictionaryContext";
import {statusIcon} from "../../../../components/Status/statusIcon";
import {FormContext} from "../../../../components/FormContext/FormContext";

export const StatusTab = (props) => {
    const { onStatusChange } = props;
    const { cargoStatuses } = useContext(DictionaryContext);
    const formContext = useContext(FormContext);

    const status = cargoStatuses.find(item => item.id == formContext.formState.statusId);

    const statusChangeHandler = (status) => {
        onStatusChange(status);
        formContext.setFormState({...formContext.formState, statusId: status.id});
    }

    return (
        <div>
            <div style={{marginTop: "17px", display: "inline-block"}}>
                {statusIcon(status ? status.icon : '')}
            </div>
            <StatusSelect statusDictionary={cargoStatuses} defaultState={status} onStatusChange={statusChangeHandler} />
        </div>
    );
}

StatusTab.propTypes = {
    onStatusChange: PropTypes.func
}
