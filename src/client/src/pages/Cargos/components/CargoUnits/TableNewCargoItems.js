import React, {useContext, useState} from "react";
import Card from "@material-ui/core/Card";
import {EnhancedTable} from "../../../../components/Table/EnhancedTable/EnhancedTable";
import {CardContent, ButtonGroup, CardHeader} from "@material-ui/core";
import {I} from "../../../../components/Icon";
import {Button} from "@material-ui/core";
import {DictionaryContext} from "../../../../components/DictionaryContext/DictionaryContext";
import {SelectAutocomplete} from "../../../../components/Autocomlete/SelectAutocomplete";
import {TextField} from "@material-ui/core";
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import {translate} from "../../../../utils/translate";

export const TableNewCargoItems = (props) => {
    const {parentCargo, parentValidate, cargoItems, setCargoItems} = props;
    //const [cargoItems, setCargoItems] = useState([]);
    const dictionaries = useContext(DictionaryContext);

    console.log('parentCargo', parentCargo);

    const headCells = [
        {sortField: 'title', numeric: false, disablePadding: true, label: 'Title'},
        {sortField: 'price', numeric: false, disablePadding: true, label: 'Price'},
        {sortField: 'cargoType', numeric: false, disablePadding: false, label: 'Cargo Type'},
        {sortField: 'exportMethodType', numeric: false, disablePadding: false, label: 'Export Type'},
        {sortField: 'sender', numeric: false, disablePadding: false, label: 'Sender'},
        {sortField: 'client', numeric: false, disablePadding: false, label: 'Client'},
        {sortField: 'customs', numeric: false, disablePadding: false, label: 'Customs'},
        {sortField: 'receiver', numeric: false, disablePadding: false, label: 'Receiver'},
    ];

    const cargoEntityChangeHandler = (entity, entityName, cargoIndex) => {
        cargoItems[cargoIndex][entityName] = entity;
        setCargoItems(cargoItems.slice(0));
    }

    const cargoTitleChangeHandler = (text, cargoIndex) => {
        cargoItems[cargoIndex].title = text;
        setCargoItems(cargoItems.slice(0));
    }

    const createRows = (item, index, parentCargo) => {
        const sender = parentCargo && parentCargo.sender ? parentCargo.sender : item.sender;
        const client = parentCargo && parentCargo.client ? parentCargo.client : item.client;
        const customs = parentCargo && parentCargo.customs ? parentCargo.customs : item.customs;
        const receiver = parentCargo && parentCargo.receiver ? parentCargo.receiver : item.receiver;
        const exportMethodType = parentCargo && parentCargo.exportMethodType ? parentCargo.exportMethodType : item.exportMethodType;
        const cargoType = parentCargo && parentCargo.cargoType ? parentCargo.cargoType : item.cargoType;

        let defaultSenderIndex = null;
        let defaultClientIndex = null;
        let defaultCustomsIndex = null;
        let defaultReceiverIndex = null;
        let defaultExportMethodTypeIndex = null;
        let defaultCargoTypeIndex = null;

        const partners = dictionaries.partners.slice(0);
        const listExportMethods = dictionaries.exportMethods.slice(0);
        const listCargoTypes = dictionaries.cargoTypes.slice(0);

        partners.map((partner, partnerIndex) => {
            if (sender && sender.id === partner.id) {
                defaultSenderIndex = partnerIndex;
            }

            if (client && client.id === partner.id) {
                defaultClientIndex = partnerIndex;
            }

            if (customs && customs.id === partner.id) {
                defaultCustomsIndex = partnerIndex;
            }

            if (receiver && receiver.id === partner.id) {
                defaultReceiverIndex = partnerIndex;
            }
        });

        listExportMethods.map((exportItemMethod, index) => {
            if (exportMethodType && exportMethodType.id === exportItemMethod.id) {
                defaultExportMethodTypeIndex = exportMethodType.id;
            }
        });

        listCargoTypes.map((cargoItemMethod, index) => {
            if (cargoType && cargoType.id === cargoItemMethod.id) {
                defaultCargoTypeIndex = cargoType.id;
            }
        });

        const createAutocomleteSelect = (items, entityName, defaultIndex) => {
            return (
                <SelectAutocomplete items={items} defaultIndex={defaultIndex} width={150} valueChangeHandler={(value) => {
                    cargoEntityChangeHandler(value, entityName, index)
                }}/>
            )
        }

        const createNativeSelect = (entityName, entityList, defaultValueId) => {
            return (
                <FormControl>
                    <Select
                        native
                        value={defaultValueId ? defaultValueId : ''}
                        onChange={(event) => {
                            const id = event.target.value;
                            const value = entityList.find(item => item.id === id);
                            cargoEntityChangeHandler(value, entityName, index)
                        }}
                    >
                        <option aria-label="None" value=""/>
                        {
                            entityList.map(item => {
                                const title = translate(JSON.parse(item.title));
                                return (
                                    <option key={item.id} value={item.id}>{title}</option>
                                );
                            })
                        }
                    </Select>
                </FormControl>
            )
        }

        return {
            id: item.id,
            title: {
                dataValue: (
                    <TextField value={item.title} label="Title" onChange={(event) => {
                        cargoEntityChangeHandler(event.currentTarget.value, 'title', index)
                    }}/>
                )
            },
            price: {
                dataValue: (
                    <TextField value={item.price} label="Price" onChange={(event) => {
                        cargoEntityChangeHandler(event.currentTarget.value, 'price', index)
                    }}/>
                )
            },
            sender: {
                dataValue: createAutocomleteSelect(partners, 'sender', defaultSenderIndex)
            },
            client: {
                dataValue: createAutocomleteSelect(partners, 'client', defaultClientIndex)
            },
            customs: {
                dataValue: createAutocomleteSelect(partners, 'customs', defaultCustomsIndex)
            },
            receiver: {
                dataValue: createAutocomleteSelect(partners, 'receiver', defaultReceiverIndex)
            },
            exportMethodType: {
                dataValue: createNativeSelect('exportMethodType', listExportMethods, defaultExportMethodTypeIndex),
            },
            cargoType: {
                dataValue: createNativeSelect('cargoType', listCargoTypes, defaultCargoTypeIndex),
            },
        }
    };

    const newCargoClickHandler = () => {

        if (!parentValidate()){
            return;
        }

        const id = cargoItems.length;
        cargoItems.push({
            id,
            title: "",
            price: "",
            sender: parentCargo?.sender,
            client: parentCargo?.client,
            customs: parentCargo?.customs,
            receiver: parentCargo?.receiver,
            exportMethodType: parentCargo?.exportMethodType,
            cargoType: parentCargo?.cargoType,
        });
        setCargoItems(cargoItems.slice(0));
    };

    const rows = cargoItems.map((item, index) => createRows(item, index, parentCargo));

    console.log('cargoItems', cargoItems);

    const actions = {
        deleteSelectedItems: (selectedItems) => {
            const newItems = cargoItems.filter(item => selectedItems.indexOf(item.id) === -1);
            setCargoItems(newItems);
        },
    };

    return (
        <>
            <Card>
                <CardHeader
                    action={
                        <ButtonGroup size="large">
                            <Button onClick={newCargoClickHandler}><I>add</I></Button>
                        </ButtonGroup>
                    }
                />
                <CardContent>
                    <EnhancedTable headCells={headCells} rows={rows} actions={actions} defaultRowsPerPage={100}/>
                </CardContent>
            </Card>
        </>
    )
}
