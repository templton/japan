import React, {useContext} from "react";
import {PropTypes} from "prop-types";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import {TableCell} from "@material-ui/core";
import Table from "@material-ui/core/Table";
import {UTableHeader} from "./UTableHeader";
import {translate} from "../../../../../utils/translate";
import {statusIcon} from "../../../../../components/Status/statusIcon";
import Checkbox from '@material-ui/core/Checkbox';
import {StatusSelect} from "../../../../../components/Status/StatusSelect";
import {DictionaryContext} from "../../../../../components/DictionaryContext/DictionaryContext";

//status(row.status.icon)

export const UTableBody = (props) => {
    const {data, openCargoModalWithCargo, onStatusChange} = props;
    const dictionaries = useContext(DictionaryContext);

    const titleClickHandle = (cargoId) => {
        openCargoModalWithCargo(cargoId);
    }

    return (
        <TableBody>
                {
                    data.map(item => {
                        const cargoType = translate(JSON.parse(item.type.title));
                        const statusIconName = item.status && item.status.icon ? item.status.icon : ''

                        console.log('statusIconName', statusIconName);

                        return (
                            <TableRow key={item.id}>
                                <TableCell>
                                    <Checkbox />
                                </TableCell>
                                <TableCell>
                                    {item.id}
                                </TableCell>
                                <TableCell>
                                    {
                                        <>
                                            <div style={{marginTop: "17px", display: "inline-block"}}>
                                                {statusIcon(statusIconName)}
                                            </div>
                                            <StatusSelect statusDictionary={dictionaries.cargoStatuses} defaultState={item.status} onStatusChange={status => {
                                                onStatusChange(status, item.id)
                                            }} />
                                        </>
                                    }

                                </TableCell>
                                <TableCell onClick={()=>titleClickHandle(item.id)} style={{color: "blue", cursor: "pointer"}}>
                                    {item.title}
                                </TableCell>
                                <TableCell>
                                    {cargoType}
                                </TableCell>
                            </TableRow>
                        )
                    })
                }
        </TableBody>
    )
};


UTableBody.propTypes = {
    data: PropTypes.array,
    openCargoModalWithCargo: PropTypes.func,
    onStatusChange: PropTypes.func
}
