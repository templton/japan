import { useMutation } from "@apollo/react-hooks";
import {loader} from 'graphql.macro';

const query = loader('./GraphQL/queryUpdateCargoAny.graphql');

export const useQueryUpdateCargoAny = () => {
    const [updateCargoAny, {loading}] = useMutation(query)
    return {updateCargoAny, loading};
}
