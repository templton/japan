import React, {useState, useEffect} from "react";
import {useParams} from "react-router-dom";
import TemplateDesktop from "../Template";
import {CargosTransferList} from "../../components/CargosTransferList/CargosTransferList";
import {useQueryCargosAll} from "../Cargos/data/useQueryCargos/useQueryCargosAll";
import CircularProgress from "@material-ui/core/CircularProgress";
import Backdrop from "@material-ui/core/Backdrop";
import {makeStyles} from "@material-ui/core/styles";
import {TextFieldForm} from "../../components/UI/text-fields/TextFieldFrom";
import {Button} from "material-ui-bootstrap";
import {useQueryUpdateCargoAny} from "./mutation/useQueryUpdateCargoAny";

const useStyles = makeStyles((theme) => ({
    backdrop: {
        zIndex: theme.zIndex.drawer + 1,
        color: '#fff',
    },
}));

const extractVoyageIdFromSearch = () => {
    const matches = window.location.search.match(/voyageId=([^&]+)/);
    return matches ? matches[1] : '';
}

export const TransferCargos = () => {
    const voyageId = extractVoyageIdFromSearch();
    const [selectedCargos, setSelectedCargos] = useState([]);
    const [cargos, setCargos] = useState([]);
    const {loading: loadingCargos, error: errorCargos, data: cargoList} = useQueryCargosAll();
    const {updateCargoAny, loading: loadingUpdateCargo} = useQueryUpdateCargoAny()
    const [filter, setFilter] = useState('');

    //<<< dialogs
    const classes = useStyles();
    const [openSnackbar, setOpenSnackbar] = React.useState(false);
    const [snakbarText, setSnakbarText] = React.useState('Data are saved successfully');
    const snackbarHandleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setOpenSnackbar(false);
    };
    const snackbarHandleClick = () => {
        setOpenSnackbar(true);
    };
    const [open, setOpenBackdor] = React.useState(false);
    const handleClose = () => {
        setOpenBackdor(false);
    };
    //>>> dialogs

    useEffect(() => {

        if (!cargoList || !cargoList.cargosAll){
            return;
        }

        const containers = cargoList.cargosAll.filter(item => {
            if (filter && filter.length && item.title.indexOf(filter) === -1){
                return false;
            }

            return (!item.parent || !item.parent.id) && (!item.voyage || item.voyage.id != voyageId);
        });

        const cargos = containers.map(item => {
            const extraFields = JSON.parse(item.extraFields);
            const frame = extraFields.frame ? extraFields.frame.value : 'NONE';
            const barcode = extraFields.barcode ? extraFields.barcode.value : 'NONE';

            return {
                id: item.id,
                title: constructTitle(item.title, frame, barcode)
            }
        })

        setCargos(cargos);

    }, [filter, cargoList]);

    if (loadingCargos) {
        return (
            <TemplateDesktop page="voyages" title="Transfer Cargo Containers">
                <Backdrop className={classes.backdrop} open={true} onClick={handleClose}>
                    <CircularProgress color="inherit"/>
                </Backdrop>
                <h2 style={{textAlign: "center"}}>Transfer cargo containers. Loading...</h2>
            </TemplateDesktop>
        )
    }

    const filterInputSubmitHandler = (filter) => {
        setFilter(filter);
    }

    const transferButtonClickHandler = async () => {

        if (!selectedCargos || !selectedCargos.length){
            alert("Nothing to save");
            return;
        }

        setOpenBackdor(true);

        const updateCargoArray = [];
        selectedCargos.map(item => {
            updateCargoArray.push({
                id: item.id,
                voyageId: voyageId
            });
        });

        for (let i in updateCargoArray) {
            let response = await updateCargoAny({
                variables: updateCargoArray[i]
            })
            let cargoData = response.data.updateCargoAny.data;
        }

        alert("Saved");
        window.location.href = "/voyages/" + voyageId;
    }

    console.log('filter = ' + filter, filter.length);
    console.log('cargos.length = ' + cargos.length);

    return (
        <TemplateDesktop page="voyages" title="Transfer Cargo Containers">
            <Backdrop className={classes.backdrop} open={open} onClick={handleClose}>
                <CircularProgress color="inherit"/>
            </Backdrop>
            <div style={{textAlign: "center"}}>
                <h2 style={{textAlign: "center"}}>Transfer cargo containers</h2>
                <div style={{textAlign: "center", width: 250, display: "inline-block"}}>
                    <TextFieldForm setText={filterInputSubmitHandler}/>
                </div>
                <CargosTransferList leftItems={cargos} left={cargos} setLeft={setCargos} right={selectedCargos}
                                    setRight={setSelectedCargos}/>
                <Button color="primary" variant="contained" onClick={transferButtonClickHandler}>Transfer to Voyage
                    #{voyageId}</Button>
            </div>
        </TemplateDesktop>
    )
}

const constructTitle = (title, frame, barcode) => {
    if (frame) {
        title += ', frame=' + frame;
    }

    if (barcode) {
        title += ', barcode=' + barcode;
    }

    return title;
}
