const host = 'https://adn.urayko.ru';

export const API_URL = {
    LOGIN: '/api/auth/login'
}

export const createUrl = (url) => {
    return host + url;
}
