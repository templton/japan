import PropTypes from "prop-types";
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import {translate} from "../../utils/translate";
import {StatusText} from "./Status";
import {statusIcon} from "./statusIcon";


const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

export const StatusSelect = (props) => {
    const {statusDictionary, defaultState, onStatusChange, showLabel} = props;

    const classes = useStyles();
    const [selectedState, setSelectedState] = React.useState(defaultState);

    const handleChange = (event) => {
        const id = event.target.value;
        const selectedItem = statusDictionary.find(item => item.id === id);
        setSelectedState(selectedItem);

        if (onStatusChange){
            onStatusChange(selectedItem);
        }
    };

    const id = 'status_' + new Date().getTime();

    return (
        <>
            <FormControl className={classes.formControl}>
                {
                    showLabel &&
                    <InputLabel htmlFor={id}>
                        Status
                    </InputLabel>
                }
                <Select
                    native
                    value={selectedState ? selectedState.id : ''}
                    onChange={handleChange}
                    inputProps={{
                        name: 'status',
                        id: id,
                    }}
                >
                    <option aria-label="None" value="" />
                    {
                        statusDictionary.map(item => {
                            const title = translate(JSON.parse(item.title));
                            return (
                                <option key={item.id} value={item.id}>{title}</option>
                            )
                        })
                    }
                </Select>
            </FormControl>
        </>
    );
}

const statusType = {
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
    show: PropTypes.bool,
    color: PropTypes.string,
    icon: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
}

StatusSelect.propTypes = {
    statusDictionary: PropTypes.arrayOf(PropTypes.shape(statusType)).isRequired,
    defaultState: PropTypes.shape(statusType),
    onStatusChange: PropTypes.func,
    showLabel: PropTypes.bool
}
