import React from "react"
import { Form, TextField, SubmitButton } from "material-ui-pack"

export const TextFieldForm = (props) => {

    const {text, setText} = props;

    const [state, setState] = React.useState({
        filter: ""
    })

    const formSubmitHandler = () => {
        setText(state.filter);
    }

    return (
        <Form
            debug={false}
            onSubmit={formSubmitHandler}
            state={state}
            setState={setState}
            busy={false}
            margin="dense"
            preventSubmitOnEnterKey
        >
            <TextField name="filter" />
            <SubmitButton>OK</SubmitButton>
        </Form>
    )
}
