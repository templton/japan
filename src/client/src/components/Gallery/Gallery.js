import React from 'react';
import Carousel from "react-material-ui-carousel"

import {
    Paper,
} from '@material-ui/core';


const  Item = (props) => {
    const {item, renderItem} = props
    return (
        <div style={{textAlign: "center"}}>
            <div style={{display: "inline-block"}}>
                <Paper>
                    {
                        renderItem
                            ? renderItem(item)
                            : <img src={item.url} alt={item.title} title={item.title} height={200} />
                    }
                </Paper>
            </div>
        </div>
    )
}

export const Gallery = (props) => {
    const {items, renderItem} = props
    return (
        <>
            {
                items && items.length &&
                <Carousel
                    className="Example"
                    autoPlay={false}
                    animation={true}
                    indicators={true}
                    cycleNavigation={true}
                    navButtonsAlwaysVisible={true}
                    navButtonsAlwaysInvisible={false}
                >
                    {items.map( (item, i) => <Item key={i} item={item} renderItem={renderItem} /> )}
                </Carousel>
            }
        </>
    )
}


