import React from "react";
import {useParams} from "react-router-dom";

export const Breadcrumbs = () => {

    const path = window.location.pathname;

    const links = [];

    const voyageIdMatches = window.location.search.match(/voyageId=([^\&]+)/);

    if (voyageIdMatches){
        links.push({
            href: '/voyages/' + voyageIdMatches[1],
            label: 'Voyage #' + voyageIdMatches[1]
        });
    }

    const regExp=/\/([^\/]+)/g;
    const items = [...path.matchAll(regExp)];

    const currentPage = path.match(/\/(\w+)\/([^\/]+)/);

    if (currentPage){
        const page = currentPage[1];
        const id = currentPage[2];

        let label = '';
        if (page == 'cargos'){
            label = page + ' #' + id;
            if (id == 'add'){
                label = "New Cargo";
            }
        }

        if (currentPage[0] == '/transfer/cargos'){
            label = "Transfer cargos";
        }

        links.push({
            href: page + '/' + id,
            label: label
        });
    }

    const countLinks = links.length;

    return (
        <ul className={`breadcrumbs`}>
            {
                links.map((item, index) => {
                  return (
                      <li key={index}>
                          {
                              index+1 === countLinks
                                  ? item.label
                                  : <a href={item.href}>{item.label}</a>
                          }
                          {
                              index+1 !== countLinks && ' > '
                          }
                      </li>
                  )
                })
            }
        </ul>
    )
}
