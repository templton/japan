import {useQuery, useLazyQuery} from "@apollo/react-hooks";
import {loader} from "graphql.macro";

const query = loader('../data/GQL/queryCargoStatuses.graphql');

export const useQueryCargoStatuses = () => {
    return useQuery(query);
}
