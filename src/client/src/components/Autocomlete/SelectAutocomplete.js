import React from "react";
import PropTypes from "prop-types";
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

export const SelectAutocomplete = (props) => {
    const {items, defaultValue, defaultIndex, valueChangeHandler, label, width} = props;
    const [value, setValue] = React.useState(defaultIndex ? items[defaultIndex] : null);

    const options = items.map((option) => {
        const firstLetter = option.title[0].toUpperCase();
        return {
            firstLetter: /[0-9]/.test(firstLetter) ? '0-9' : firstLetter,
            ...option,
        };
    });

    return (
        <>
            <Autocomplete
                value={value}
                onChange={(event, newValue) => {
                    setValue(newValue);
                    valueChangeHandler(newValue);
                }}
                options={options.sort((a, b) => -b.firstLetter.localeCompare(a.firstLetter))}
                groupBy={(option) => option.firstLetter}
                getOptionLabel={(option) => {
                    return option.title ? option.title : '';
                }}
                getOptionSelected={option => {
                    return option;
                }}
                style={{ width: width ? width : 300 }}
                renderInput={(params) => <TextField {...params} label={label} variant="outlined" />}
            />
        </>
    )
};

SelectAutocomplete.propTypes = {
    items: PropTypes.arrayOf(Object).isRequired,
    valueChangeHandler: PropTypes.func,
    // Экземпляр элемента item
    //defaultValue: PropTypes.object,
    defaultIndex: PropTypes.number,
    label: PropTypes.string
}
