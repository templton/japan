import React from 'react';
import ImageUploading from 'react-images-uploading';
import {withModalStyle} from "../../pages/Cargos/components/withModalStyle/withModalStyle";

export const Uploader = (props) => {
    const {images, setImages, storeFile} = props;
    const maxNumber = 69;
    const imageUploadFilesChange = (imageList, addUpdateIndex) => {
        storeFile(imageList[addUpdateIndex]);
        // data for submit
        // console.log('data for submit');
        // console.log(imageList, addUpdateIndex);
        setImages(imageList);
    };

    return (
        <ImageUploading
            multiple
            value={images}
            onChange={imageUploadFilesChange}
            maxNumber={maxNumber}
            dataURLKey="data_url"
        >
            {({
                  imageList,
                  onImageUpload,
                  onImageRemoveAll,
                  onImageUpdate,
                  onImageRemove,
                  isDragging,
                  dragProps,
              }) => (
                // write your building UI
                <div className="upload__image-wrapper">
                    <div>
                        <button
                            style={isDragging ? { color: 'red' } : undefined}
                            onClick={onImageUpload}
                            {...dragProps}
                        >
                            Click or Drop here
                        </button>
                    </div>
                    <div style={{marginTop: 15}}>
                        <button onClick={onImageRemoveAll}>Remove all UPLOADED images</button>
                    </div>
                    {imageList.map((image, index) => (
                        <div key={index} className="image-item">
                            <img src={image['data_url']} alt="" width="100" />
                            <div className="image-item__btn-wrapper">
                                <button onClick={() => onImageUpdate(index)}>Update</button>
                                <button onClick={() => onImageRemove(index)}>Remove</button>
                            </div>
                        </div>
                    ))}
                </div>
            )}
        </ImageUploading>
    )
}

export const StyleUploader = withModalStyle(Uploader);
