import ApolloClient from 'apollo-client'
import { createUploadLink } from 'apollo-upload-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import {getUserIdFromStorage} from "../hooks/auth.hook";

const userIdFromStorage = getUserIdFromStorage();

const httpLink = createUploadLink({
    uri: 'http://amixline.loc/api?userId=' + userIdFromStorage,
});

export const client = new ApolloClient({
    link: httpLink,
    cache: new InMemoryCache(),
});
