#!/bin/bash

APP_PATH="$(pwd)/zebrapizza"
APP_NAME="ZebraPIZZA"
APP_ENV="local"
APP_DEBUG="true"
APP_URL="https://localhost"
DB_CONNECTION="mysql"
DB_HOST="127.0.0.1"
DB_PORT="3306"
DB_DATABASE="pizza"
DB_USERNAME="root"
DB_PASSWORD=""
MAIL_MAILER="smtp"
MAIL_HOST="smtp.example.com"
MAIL_PORT="25"
MAIL_USERNAME="user@example.com"
MAIL_PASSWORD="password"
MAIL_ENCRYPTION="SSL"
MAIL_FROM_ADDRESS=$MAIL_USERNAME
MAIL_FROM_NAME="ZebraPIZZA"
SMSCRU_SENDER="SMSC.RU"
SMSCRU_LOGIN="login"
SMSCRU_SECRET="password"

echo "Type path to project directory"
echo "or press enter for [${APP_PATH}]"
read -r value
if [ -n "$value" ]; then APP_PATH=$value; fi

git clone https://sergeysuprunov@bitbucket.org/sergeysuprunov/zebrapizza.git "$APP_PATH" >> install.log
cd "$APP_PATH" || exit #TODO Добавить вывод ошибок и выход
echo "installing dependencies..."
composer install
npm install

echo "Configure App"
echo "Type App url"
echo "or press enter for [${APP_URL}]"
read -r value
if [ -n "$value" ]; then APP_URL=$value; fi

echo "Type DB_HOST"
echo "or press enter for [${DB_HOST}]"
read -r value
if [ -n "$value" ]; then DB_HOST=$value; fi


echo "Type DB_PORT"
echo "or press enter for [${DB_PORT}]"
read -r value
if [ -n "$value" ]; then DB_PORT=$value; fi

echo "Type DB_DATABASE"
echo "or press enter for [${DB_DATABASE}]"
read -r value
if [ -n "$value" ]; then DB_DATABASE=$value; fi

echo "Type DB_USERNAME"
echo "or press enter for [${DB_USERNAME}]"
read -r value
if [ -n "$value" ]; then DB_USERNAME=$value; fi

echo "Type DB_PASSWORD"
echo "or press enter for [${DB_PASSWORD}]"
read -r value
if [ -n "$value" ]; then DB_PASSWORD=$value; fi

echo "Type MAIL_HOST"
echo "or press enter for [${MAIL_HOST}]"
read -r value
if [ -n "$value" ]; then MAIL_HOST=$value; fi

echo "Type MAIL_PORT"
echo "or press enter for [${MAIL_PORT}]"
read -r value
if [ -n "$value" ]; then MAIL_PORT=$value; fi

echo "Type MAIL_USERNAME"
echo "or press enter for [${MAIL_USERNAME}]"
read -r value
if [ -n "$value" ]; then MAIL_USERNAME=$value; fi

echo "Type MAIL_PASSWORD"
echo "or press enter for [${MAIL_PASSWORD}]"
read -r value
if [ -n "$value" ]; then MAIL_PASSWORD=$value; fi

echo "Type MAIL_ENCRYPTION"
echo "or press enter for [${MAIL_ENCRYPTION}]"
read -r value
if [ -n "$value" ]; then MAIL_ENCRYPTION=$value; fi

echo "Type MAIL_FROM_ADDRESS"
echo "or press enter for [${MAIL_FROM_ADDRESS}]"
read -r value
if [ -n "$value" ]; then MAIL_FROM_ADDRESS=$value; fi

echo "Type MAIL_FROM_NAME"
echo "or press enter for [${MAIL_FROM_NAME}]"
read -r value
if [ -n "$value" ]; then MAIL_FROM_NAME=$value; fi

echo "Type SMSCRU_LOGIN"
echo "or press enter for [${SMSCRU_LOGIN}]"
read -r value
if [ -n "$value" ]; then SMSCRU_LOGIN=$value; fi

echo "Type SMSCRU_SECRET"
echo "or press enter for [${SMSCRU_SECRET}]"
read -r value
if [ -n "$value" ]; then SMSCRU_SECRET=$value; fi

echo "APP_NAME=${APP_NAME}" >> .env
echo "APP_ENV=${APP_ENV}" >> .env
echo "APP_DEBUG=${APP_DEBUG}" >> .env
echo "APP_URL=${APP_URL}" >> .env
echo -e "APP_KEY=\n" >> .env
echo "DB_CONNECTION=${DB_CONNECTION}" >> .env
echo "DB_HOST=${DB_HOST}" >> .env
echo "DB_PORT=${DB_PORT}" >> .env
echo "DB_DATABASE=${DB_DATABASE}" >> .env
echo "DB_USERNAME=${DB_USERNAME}" >> .env
echo -e "DB_PASSWORD=${DB_PASSWORD}\n" >> .env
echo "MAIL_MAILER=${MAIL_MAILER}" >> .env
echo "MAIL_HOST=${MAIL_HOST}" >> .env
echo "MAIL_PORT=${MAIL_PORT}" >> .env
echo "MAIL_USERNAME=${MAIL_USERNAME}" >> .env
echo "MAIL_PASSWORD=${MAIL_PASSWORD}" >> .env
echo "MAIL_ENCRYPTION=${MAIL_ENCRYPTION}" >> .env
echo "MAIL_FROM_ADDRESS=${MAIL_FROM_ADDRESS}" >> .env
echo -e "MAIL_FROM_NAME=${MAIL_FROM_NAME}\n" >> .env
echo "SMSCRU_SENDER=${SMSCRU_SENDER}" >> .env
echo "SMSCRU_LOGIN=${SMSCRU_LOGIN}" >> .env
echo "SMSCRU_SECRET=${SMSCRU_SECRET}" >> .env

php artisan key:generate
php artisan jwt:secret
php artisan migrate:fresh --seed


# TODO: open install.log
# TODO: composer init
